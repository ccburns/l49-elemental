jQuery(document).ready(function($) {

   $('#Form_ItemEditForm_TestimonialMarkup_Holder').hide();

   $(document).on("click", ".add-testimonial-button", function() {
      var Content = $('#Form_ItemEditForm_ContentField').val();
      var Name = $('#Form_ItemEditForm_NameField').val();
      var Company = $('#Form_ItemEditForm_CompanyField').val();
      var LastItemPosition = $('#TestimonialElementContainer table').find('tr').size();
      var TestimonialContent = "<tr class=\"testimonial-item-"+LastItemPosition+"\" data-position=\""+LastItemPosition+"\"><td class=\"testimonial-name\">"+Name+"</td><td class=\"testimonial-company\">"+Company+"</td><td class=\"testimonial-content\">"+Content+"</td><td><a href=\"#\" class=\"testimonial-edit\">Edit</a></td><td><a href=\"#\" class=\"testimonial-delete\">Delete</a></td></tr>"
      $('#TestimonialElementContainer table tbody').append(TestimonialContent);
      $('#TestimonialElementContainer').show();
      $('#Form_ItemEditForm_NameField').val('');
      $('#Form_ItemEditForm_CompanyField').val('');
      $('#Form_ItemEditForm_ContentField').val('');
      window.parent.tinyMCE.activeEditor.setContent('');
      CreateTestimonialJSON();
   });

   $(document).on("click", ".update-testimonial-button", function() {
      var Content = $('#Form_ItemEditForm_ContentField').val();
      var Name = $('#Form_ItemEditForm_NameField').val();
      var Company = $('#Form_ItemEditForm_CompanyField').val();
      var ItemPosition = $(this).attr('data-position');
      var TestimonialContent = "<td class=\"testimonial-name\">"+Name+"</td><td class=\"testimonial-company\">"+Company+"</td><td class=\"testimonial-content\">"+Content+"</td><td><a href=\"#\" class=\"testimonial-edit\">Edit</a></td><td><a href=\"#\" class=\"testimonial-delete\">Delete</a></td>"
      $('#TestimonialElementContainer table tr.testimonial-item-'+ItemPosition).html(TestimonialContent);
      $('#TestimonialElementContainer').show();
      $('#Form_ItemEditForm_NameField').val('');
      $('#Form_ItemEditForm_CompanyField').val('');
      $('#Form_ItemEditForm_ContentField').val('');
      window.parent.tinyMCE.activeEditor.setContent('');
      CreateTestimonialJSON();
      $(".update-testimonial-button").hide();
      $(".add-testimonial-button").show();
   });

   $(document).on("click", ".testimonial-delete", function(e) {
      e.preventDefault();
      $(this).parent().parent().remove();
      CreateTestimonialJSON();
   });

   $(document).on("click", ".testimonial-edit", function(e) {
      e.preventDefault();
      var Name = $(this).parent().parent().find('.testimonial-name').html();
      var Company = $(this).parent().parent().find('.testimonial-company').html();
      var Content = $(this).parent().parent().find('.testimonial-content').html();
      var Position = $(this).parent().parent().data('position');
      $('.add-testimonial-button').hide();
      $('.update-testimonial-button').show();
      $('.update-testimonial-button').attr('data-position', Position);
      $('#Form_ItemEditForm_NameField').val(Name);
      $('#Form_ItemEditForm_CompanyField').val(Company);
      $('#Form_ItemEditForm_ContentField').val(Content);
      window.parent.tinyMCE.activeEditor.setContent(Content);
      CreateTestimonialJSON();
   });

   function CreateTestimonialJSON() {
      var TestimonialMarkup = '';
      $('#TestimonialElementContainer table tbody tr').each(function(index){
         var Name = $(this).find('.testimonial-name').html();
         var Company = $(this).find('.testimonial-company').html();
         var Content = $(this).find('.testimonial-content').html();
            TestimonialMarkup += '{' +
                 '"name":"'+Name.replace(/"/g, '\\"')+'",' +
                 '"company":"'+Company.replace(/"/g, '\\"')+'",' +
                 '"content":"'+Content.replace(/"/g, '\\"')+'"' +
                 '},';
      });
      $('#Form_ItemEditForm_TestimonialMarkup').val(TestimonialMarkup.slice(0, -1));
   }

});