jQuery(document).ready(function($) {

   $('#Form_ItemEditForm_TimelineMarkup_Holder').hide();

   $(document).on("click", ".add-timeline-button", function() {
      var Date = $('#Form_ItemEditForm_DateField').val();
      var Content = $('#Form_ItemEditForm_ContentField').val();
      var BackgroundField = $('#Form_ItemEditForm_BackgroundField').val();
      var StyleField = $('#Form_ItemEditForm_StyleField').val();
      var LastItemPosition = $('#TimelineBlockElementContainer table').find('tr').size();
      //var LastItemPosition = ($('#TimelineBlockElementContainer table').data('total-items')+1);
      var TimelineContent = "<tr class=\"timeline-item-"+LastItemPosition+"\" data-position=\""+LastItemPosition+"\">" +
          "<td class=\"timeline-date\">"+Date+"</td>" +
          "<td class=\"timeline-content\">"+Content+"</td>" +
          "<td class=\"timeline-background-type\">"+BackgroundField+"</td>" +
          "<td class=\"timeline-background-style\">"+StyleField+"</td>" +
          "<td><a href=\"#\" class=\"timeline-edit\">Edit</a></td>" +
          "<td><a href=\"#\" class=\"timeline-delete\">Delete</a></td>" +
          "</tr>"
      $('#TimelineBlockElementContainer table tbody').append(TimelineContent);
      $('#TimelineBlockElementContainer').show();
      $('#Form_ItemEditForm_DateField').val('');
      $('#Form_ItemEditForm_ContentField').val('');
      $('#Form_ItemEditForm_BackgroundField').val('');
      $('#Form_ItemEditForm_StyleField').val('');
      window.parent.tinyMCE.activeEditor.setContent('');
      CreateTimelineJSON();
   });

   $(document).on("click", ".update-timeline-button", function() {
      var Date = $('#Form_ItemEditForm_DateField').val();
      var Content = $('#Form_ItemEditForm_ContentField').val();
      var BackgroundField = $('#Form_ItemEditForm_BackgroundField').val();
      var StyleField = $('#Form_ItemEditForm_StyleField').val();
      var ItemPosition = $(this).attr('data-position');
      var TimelineContent = "<td class=\"timeline-date\">"+Date+"</td>" +
          "<td class=\"timeline-content\">"+Content+"</td>" +
          "<td class=\"timeline-background-type\">"+BackgroundField+"</td>" +
          "<td class=\"timeline-background-style\">"+StyleField+"</td>" +
          "<td><a href=\"#\" class=\"timeline-edit\">Edit</a></td>" +
          "<td><a href=\"#\" class=\"timeline-delete\">Delete</a></td>";
      $('#TimelineBlockElementContainer table tr.timeline-item-'+ItemPosition).html(TimelineContent);
      $('#TimelineBlockElementContainer table').show();
      $('#Form_ItemEditForm_LabelField').val('');
      $('#Form_ItemEditForm_ContentField').val('');
      $('#Form_ItemEditForm_BackgroundField').val('');
      $('#Form_ItemEditForm_StyleField').val('');
      window.parent.tinyMCE.activeEditor.setContent('');
      CreateTimelineJSON();
      $(".update-timeline-button").hide();
      $(".add-timeline-button").show();
   });

   $(document).on("click", ".timeline-delete", function(e) {
      e.preventDefault();
      $(this).parent().parent().remove();
      CreateTimelineJSON();
   });

   $(document).on("click", ".timeline-edit", function(e) {
      e.preventDefault();
      var Date = $(this).parent().parent().find('.timeline-date').html();
      var Content = $(this).parent().parent().find('.timeline-content').html();
      var BackgroundField = $(this).parent().parent().find('.timeline-background-type').html();
      var StyleField = $(this).parent().parent().find('.timeline-background-style').html();
      var Position = $(this).parent().parent().data('position');
      $('.add-timeline-button').hide();
      $('.update-timeline-button').show().attr('data-position', Position);
      $('#Form_ItemEditForm_DateField').val(Date);
      $('#Form_ItemEditForm_ContentField').val(Content);
      $('#Form_ItemEditForm_BackgroundField').val(BackgroundField);
      $('#Form_ItemEditForm_StyleField').val(StyleField);
      window.parent.tinyMCE.activeEditor.setContent(Content);
      CreateTimelineJSON();
   });

   function CreateTimelineJSON() {
      var TimelineMarkup = '';
      $('#TimelineBlockElementContainer table tbody tr').each(function(index){
         var Date = $(this).find('.timeline-date').html();
         var Content = $(this).find('.timeline-content').html();
         var BackgroundType = $(this).find('.timeline-background-type').html();
         var StyleField = $(this).find('.timeline-background-style').html();
         TimelineMarkup += '{' +
            '"date":"'+Date.replace(/"/g, '\\"')+'",' +
            '"content":"'+Content.replace(/"/g, '\\"')+'",' +
            '"background-type":"'+BackgroundType.replace(/"/g, '\\"')+'",' +
            '"background-style":"'+StyleField.replace(/"/g, '\\"')+'"' +
           '},';
      });
      $('#Form_ItemEditForm_TimelineMarkup').val(TimelineMarkup.slice(0, -1));
   }

});