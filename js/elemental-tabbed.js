jQuery(document).ready(function($) {

   $('#Form_ItemEditForm_TabbedMarkup_Holder').hide();

   $(document).on("click", ".add-tabbed-button", function() {
      var Label = $('#Form_ItemEditForm_LabelField').val();
      var Icon = $('#Form_ItemEditForm_IconField').val();
      var Content = $('#Form_ItemEditForm_ContentField').val();
      var LastItemPosition = $('#TabbedBlockElementContainer table').find('tr').size();
      var TabbedContent = "<tr class=\"tabbed-item-"+LastItemPosition+"\" data-position=\""+LastItemPosition+"\"><td class=\"tabbed-label\">"+Label+"</td><td class=\"tabbed-icon\">"+Icon+"</td><td class=\"tabbed-content\">"+Content+"</td><td><a href=\"#\" class=\"tabbed-edit\">Edit</a></td><td><a href=\"#\" class=\"tabbed-delete\">Delete</a></td></tr>"
      $('#TabbedBlockElementContainer table tbody').append(TabbedContent);
      $('#TabbedBlockElementContainer').show();
      $('#Form_ItemEditForm_LabelField').val('');
      $('#Form_ItemEditForm_IconField').val('');
      $('#Form_ItemEditForm_ContentField').val('');
      window.parent.tinyMCE.activeEditor.setContent('');
      CreateTabbedJSON();
   });

   $(document).on("click", ".update-tabbed-button", function() {
      var Label = $('#Form_ItemEditForm_LabelField').val();
      var Icon = $('#Form_ItemEditForm_IconField').val();
      var Content = $('#Form_ItemEditForm_ContentField').val();
      var ItemPosition = $(this).attr('data-edit-position');
      var TabbedContent = "<td class=\"tabbed-label\">"+Label+"</td><td class=\"tabbed-icon\">"+Icon+"</td><td class=\"tabbed-content\">"+Content+"</td><td><a href=\"#\" class=\"tabbed-edit\">Edit</a></td><td><a href=\"#\" class=\"tabbed-delete\">Delete</a></td>";
      $('#TabbedBlockElementContainer table tr.tabbed-item-'+ItemPosition).html(TabbedContent);
      $('#TabbedBlockElementContainer').show();
      $('#Form_ItemEditForm_LabelField').val('');
      $('#Form_ItemEditForm_IconField').val('');
      $('#Form_ItemEditForm_ContentField').val('');
      window.parent.tinyMCE.activeEditor.setContent('');
      CreateTabbedJSON();
      $(".update-tabbed-button").hide();
      $(".add-tabbed-button").show();
   });

   $(document).on("click", ".tabbed-delete", function(e) {
      e.preventDefault();
      $(this).parent().parent().remove();
      CreateTabbedJSON();
   });

   $(document).on("click", ".tabbed-edit", function(e) {
      e.preventDefault();
      var Label = $(this).parent().parent().find('.tabbed-label').html();
      var Icon = $(this).parent().parent().find('.tabbed-icon').html();
      var Content = $(this).parent().parent().find('.tabbed-content').html();
      var Position = $(this).parent().parent().data('position');
      $('.add-tabbed-button').hide();
      $('.update-tabbed-button').show().attr('data-edit-position', Position);
      $('#Form_ItemEditForm_LabelField').val(Label);
      $('#Form_ItemEditForm_IconField').val(Icon);
      $('#Form_ItemEditForm_ContentField').val(Content);
      window.parent.tinyMCE.activeEditor.setContent(Content);
      CreateTabbedJSON();
   });

   function CreateTabbedJSON() {
      var TabbedMarkup = '';
      $('#TabbedBlockElementContainer table tbody tr').each(function(index){
         var Label = $(this).find('.tabbed-label').html();
         var Icon = $(this).find('.tabbed-icon').html();
         var Content = $(this).find('.tabbed-content').html();
         TabbedMarkup += '{' +
                 '"label":"'+Label.replace(/"/g, '\\"')+'",' +
                 '"icon":"'+Icon.replace(/"/g, '\\"')+'",' +
                 '"content":"'+Content.replace(/"/g, '\\"')+'"' +
                 '},';
      });
      $('#Form_ItemEditForm_TabbedMarkup').val(TabbedMarkup.slice(0, -1));
   }

});