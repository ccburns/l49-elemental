jQuery(document).ready(function($) {

   $('#Form_ItemEditForm_AccordianMarkup_Holder').hide();

   $(document).on("click", ".add-accordian-button", function() {
      var Label = $('#Form_ItemEditForm_LabelField').val();
      var Content = $('#Form_ItemEditForm_ContentField').val();
      var LastItemPosition = $('#AccordianElementContainer table').find('tr').size();
      var AccordianContent = "<tr class=\"accordian-item-"+LastItemPosition+"\" data-position=\""+LastItemPosition+"\"><td class=\"accordian-label\">"+Label+"</td><td class=\"accordian-content\">"+Content+"</td><td><a href=\"#\" class=\"accordian-edit\">Edit</a></td><td><a href=\"#\" class=\"accordian-delete\">Delete</a></td></tr>"
      $('#AccordianElementContainer table tbody').append(AccordianContent);
      $('#AccordianElementContainer').show();
      $('#Form_ItemEditForm_LabelField').val('');
      $('#Form_ItemEditForm_ContentField').val('');
      window.parent.tinyMCE.activeEditor.setContent('');
      CreateAccordianJSON();
   });

   $(document).on("click", ".update-accordian-button", function() {
      var Label = $('#Form_ItemEditForm_LabelField').val();
      var Content = $('#Form_ItemEditForm_ContentField').val();
      var ItemPosition = $(this).attr('data-position');
      var AccordianContent = "<td class=\"accordian-label\">"+Label+"</td><td class=\"accordian-content\">"+Content+"</td><td><a href=\"#\" class=\"accordian-edit\">Edit</a></td><td><a href=\"#\" class=\"accordian-delete\">Delete</a></td>";
      $('#AccordianElementContainer table tr.accordian-item-'+ItemPosition).html(AccordianContent);
      $('#AccordianElementContainer table').show();
      $('#Form_ItemEditForm_LabelField').val('');
      $('#Form_ItemEditForm_ContentField').val('');
      window.parent.tinyMCE.activeEditor.setContent('');
      CreateAccordianJSON();
      $(".update-accordian-button").hide();
      $(".add-accordian-button").show();
   });

   $(document).on("click", ".accordian-delete", function(e) {
      e.preventDefault();
      $(this).parent().parent().remove();
      CreateAccordianJSON();
   });

   $(document).on("click", ".accordian-edit", function(e) {
      e.preventDefault();
      var Label = $(this).parent().parent().find('.accordian-label').html();
      var Content = $(this).parent().parent().find('.accordian-content').html();
      var Position = $(this).parent().parent().data('position');
      $('.add-accordian-button').hide();
      $('.update-accordian-button').show();
      $('.update-accordian-button').attr('data-position', Position);
      $('#Form_ItemEditForm_LabelField').val(Label);
      $('#Form_ItemEditForm_ContentField').val(Content);
      window.parent.tinyMCE.activeEditor.setContent(Content);
      CreateAccordianJSON();
   });

   function CreateAccordianJSON() {
      var AccordianMarkup = '';
      $('#AccordianElementContainer table tbody tr').each(function(index){
         var Label = $(this).find('.accordian-label').html();
         var Content = $(this).find('.accordian-content').html();
            AccordianMarkup += '{' +
                 '"label":"'+Label.replace(/"/g, '\\"')+'",' +
                 '"content":"'+Content.replace(/"/g, '\\"')+'"' +
                 '},';
      });
      $('#Form_ItemEditForm_AccordianMarkup').val(AccordianMarkup.slice(0, -1));
   }

});