<div class="element_content__content <% if $Style %>element_content__$CssStyle<% end_if %>">
	<% if $BlockHeader %>
	<div class="title-box">
		<h2 class="title h1">{$BlockHeader}</h2>
	</div>
	<% end_if %>
	<% if $Link %>
		<a href="{$Link}" class="icon-content-link">
			{$IconCode.RAW}
			{$Paragraph}
		</a>
	<% else %>
		{$IconCode.RAW}
		{$Paragraph}
	<% end_if %>
</div>