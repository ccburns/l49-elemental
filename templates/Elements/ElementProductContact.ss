<div class="<% if $ExtraClass %>{$ExtraClass}<% else %>col-xs-12 col-sm-12 col-md-3<% end_if %>">
	<header class="page-header typography">
		<div class="container">
			<h3 class="title h3">Contact Us</h3>
		</div>
	</header>
	<p>
		<% if $EmailAddress %>E: <a href="mailto:{$EmailAddress}">$EmailAddress</a><br><% end_if %>
		<% if $PhoneNumber %>P: <a href="tel:{$PhoneNumber}">$PhoneNumber</a><% end_if %>
	</p>

	<% if $ItemID %>
	<ul class="product-contact-buttons">
		<li class="product-request-a-quote">
			<a href="/products/RequestAQuote/{$ItemID}/" class="fancyboxajaxquote">
				Request a quote or demo
			</a>
		</li>
		<li class="contact-a-specialist">
			<a href="/products/ContactASpecialist/{$ItemID}/" class="fancyboxajaxcontact">
				Contact a Specialist
			</a>
		</li>
	</ul> <% end_if %>
</div>