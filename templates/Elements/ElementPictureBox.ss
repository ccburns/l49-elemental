<div class="element_content__content <% if $Style %>element_content__$CssStyle<% end_if %> picture-box-element col-xs-12 col-sm-12 col-md-12" style="background: #f4f4f4 url({$BackgroundImage.Url}) no-repeat center center;height:{$BackgroundImage.Height}px">
    <div class="container">
        <div class="row">
            <div class="col-xs-4 col-sm-4 col-md-4 picture-box-container" style="padding: {$TopPadding}px 0 {$BottomPadding}px 0;margin-top: {$TopMargin}px;">
                <span class="picture-box-label">{$LabelOne}</span>
                <span class="picture-box-description">{$DescriptionOne}</span>
            </div>
            <div class="col-xs-4 col-sm-4 col-md-4 picture-box-container" style="padding: {$TopPadding}px 0 {$BottomPadding}px 0;margin-top: {$TopMargin}px;">
                <span class="picture-box-label">{$LabelTwo}</span>
                <span class="picture-box-description">{$DescriptionTwo}</span>
            </div>
            <div class="col-xs-4 col-sm-4 col-md-4 picture-box-container" style="padding: {$TopPadding}px 0 {$BottomPadding}px 0;margin-top: {$TopMargin}px;">
                <span class="picture-box-label">{$LabelThree}</span>
                <span class="picture-box-description">{$DescriptionThree}</span>
            </div>
        </div>
    </div>
</div>