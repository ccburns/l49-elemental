<?php

/**
 * @package elemental
 */
class EmbedContent extends BaseElement
{

    private static $db = array(
        'EmbedCode' => 'Text',
        'FullWidth' => 'Boolean',
        'Style' => 'Varchar'
    );

    private static $styles = array();

    private static $title = "Embed Code";

    private static $description = "Block of embedded RAW HTML (google maps / youtube).";

    public function getCMSFields()
    {

        $styles = $this->config()->get('styles');

        if (count($styles) > 0) {
            $this->beforeUpdateCMSFields(function ($fields) use ($styles) {
                $fields->addFieldsToTab('Root.Main', new CheckboxField('FullWidth', 'Check if you want this element to be full width (is not inside a .container class)'));
                $fields->addFieldsToTab('Root.Main', new TextareaField('EmbedCode', 'RAW Embed Code'));

                $styles->setEmptyString('Select a custom style..');
            });
        } else {
            $this->beforeUpdateCMSFields(function ($fields) {
                $fields->removeByName('Style');
            });
        }

        $fields = parent::getCMSFields();

        if ($this->isEndofLine('ElementContent') && $this->hasExtension('VersionViewerDataObject')) {
            $fields = $this->addVersionViewer($fields, $this);
        }

        return $fields;
    }

    public function getCssStyle()
    {
        $styles = $this->config()->get('styles');
        $style = $this->Style;

        if (isset($styles[$style])) {
            return strtolower($styles[$style]);
        }
    }
}
