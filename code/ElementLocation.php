<?php

/**
 * @package elemental
 */
class ElementLocation extends BaseElement
{

    private static $db = array(
        'BlockHeader' => 'Varchar(255)',
        'HTML' => 'HTMLText',
        'MapEmbedCode' => 'Text',
        'DisplayAsColumns' => 'Boolean',
        'Style' => 'Varchar'
    );

    private static $styles = array();

    private static $title = "Location Block";

    private static $description = "Map (via Embed Code) with contact details on the right";

    public function getCMSFields()
    {

        $this->beforeUpdateCMSFields(function ($fields) {

            $fields->addFieldsToTab('Root.Main', CheckboxField::create('DisplayAsColumns', 'Display As Columns')->setDescription('By Default the map is stacked on top of the Content area. If checked these two elements will be side by side'));
            $fields->addFieldsToTab('Root.Main', new HtmlEditorField('HTML', 'Content'));
            $fields->addFieldsToTab('Root.Main', new TextareaField('MapEmbedCode', 'Map Embed Code', 10));
            $fields->removeByName('Style');
        });

        $fields = parent::getCMSFields();

        if ($this->isEndofLine('ElementContent') && $this->hasExtension('VersionViewerDataObject')) {
            $fields = $this->addVersionViewer($fields, $this);
        }

        return $fields;
    }

    public function getCssStyle()
    {
        $styles = $this->config()->get('styles');
        $style = $this->Style;

        if (isset($styles[$style])) {
            return strtolower($styles[$style]);
        }
    }
}
