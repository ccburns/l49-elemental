<?php

/**
 * @package elemental
 */
class ElementFullWidthImage extends BaseElement
{

    private static $db = array(
        'InternalExternal'=>'Varchar(255)',
        'InternalLink'=>'Varchar(255)',
        'ExternalLink'=>'Varchar(255)',
        'OpenNewWindow'=>'Boolean'
    );

    private static $has_one = array(
        'Page' => 'Page',
        'ElementFeatureImage' => 'Image'
    );

    private static $styles = array();

    private static $title = "Full Width Image";

    private static $description = "This block will allow you to configure a Full Width Image Block";

    public function getCMSFields()
    {

        $this->beforeUpdateCMSFields(function ($fields) {
            $fields->removeByName('Style');

            $PageID = Controller::curr()->currentPageID();

            $fields->addFieldToTab("Root.Main", HeaderField::create('Header1', 'Link for Feature Image', 3));
            $fields->addFieldToTab("Root.Main", HiddenField::create('PageID', 'PageID', $PageID));
            $fields->addFieldToTab("Root.Main", OptionsetField::create('InternalExternal', 'Internal or External Link', array('NoLink' => 'No Link on Banner Image', 'Internal' => 'Internal (link to a page on your website)', 'External' => 'External (link to an external website)')));
            $fields->addFieldToTab("Root.Main", TreeDropdownField::create('InternalLink', 'Internal Link', 'SiteTree')->setDescription('Please enter the full URL include the http://'));
            $fields->addFieldToTab("Root.Main", TextField::create('ExternalLink', 'External Link (include full URL http://domain.com)')->setDescription('Please enter the full URL'));
            $fields->addFieldToTab("Root.Main", CheckboxField::create('OpenNewWindow', 'Check this box to open the link in a new window'));
            $fields->addFieldToTab("Root.Main", LiteralField::create('Literal1', '<p class="message warning">Feature Image (Dimensions: 1600px wide x 480px high [Height can be whatever you want it to be].)</p>'));
            $UploadFeatureImage = new UploadField('ElementFeatureImage', 'Upload a feature Image for this section (Dimensions: 1600px wide x 480px high)');
            $UploadFeatureImage->setFolderName('theme-images/feature-images');
            $fields->addFieldsToTab('Root.Main', $UploadFeatureImage);

        });

        $fields = parent::getCMSFields();

        if ($this->isEndofLine('ElementContent') && $this->hasExtension('VersionViewerDataObject')) {
            $fields = $this->addVersionViewer($fields, $this);
        }

        return $fields;
    }

    public function getCssStyle()
    {
        $styles = $this->config()->get('styles');
        $style = $this->Style;

        if (isset($styles[$style])) {
            return strtolower($styles[$style]);
        }
    }

}