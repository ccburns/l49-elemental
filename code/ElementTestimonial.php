<?php

/**
 * @package elemental
 */
class ElementTestimonial extends BaseElement
{

    private static $db = array(
        'BlockHeader' => 'Varchar(255)',
        'Autoscroll' => 'Boolean',
        'AutoscrollSpeed' => 'Int',
        'Navigation' => 'Boolean',
        'Pagination' => 'Boolean',
        'TestimonialMarkup' => 'HTMLText',
        'Style' => 'Varchar'
    );

    private static $styles = array();

    private static $title = "Testimonial Block";

    private static $description = "This block will allow you to configure an series of rotating Testimonials";

    public function getCMSFields()
    {

        $this->beforeUpdateCMSFields(function ($fields) {

            $fields->removeByName('Style');
            $fields->addFieldsToTab('Root.Main', new HeaderField('Header1', 'Testimonial controls', 3), 'Autoscroll');
            $fields->addFieldsToTab('Root.Main', new CheckboxField('Autoscroll', 'Autoscroll if there is more than one testimonial', 3));
            $fields->addFieldsToTab('Root.Main', DropdownField::create('AutoscrollSpeed', 'Autoscroll speed in seconds', array(1000 => '1 second',2000 => '2 seconds',3000 => '3 seconds',4000 => '4 seconds',5000 => '5 seconds',6000 => '6 seconds',7000 => '7 seconds',8000 => '8 seconds',9000 => '9 seconds',10000 => '10 seconds',15000 => '15 seconds',30000 => '30 seconds',60000 => '1 minute'))->setValue('5 seconds'));
            $fields->addFieldsToTab('Root.Main', new CheckboxField('Pagination', 'Display pagination bullets underneath the testimonial', 3));
            $fields->addFieldsToTab('Root.Main', new CheckboxField('Navigation', 'Display navigation arrows in the top right of the section', 3));
            $fields->addFieldsToTab('Root.Main', new HeaderField('Header2', 'Testimonial Content Instructions', 3));
            $fields->addFieldsToTab('Root.Main', new LiteralField('Literal1', '<p>Fill in the Label and Content fields below and then click the <strong><em>Add Testimonial Item</em></strong> button below it to add this content to the Rotating Testimonials. Once clicked you will see the details in the table below the Button.</p>'));
            $fields->addFieldsToTab('Root.Main', new HtmlEditorField('ContentField', 'Testimonial Content'));
            $fields->addFieldsToTab('Root.Main', new TextField('NameField', 'Full Name'));
            $fields->addFieldsToTab('Root.Main', new TextareaField('CompanyField', 'Company Name'));
            $fields->addFieldsToTab('Root.Main', new LiteralField('Button1', '<button class="add-testimonial-button" id="AddTestimonialButton">Add Testimonial Item</button>'));
            $fields->addFieldsToTab('Root.Main', new LiteralField('Button2', '<button class="update-testimonial-button" id="UpdateTestimonialButton">Update Testimonial Item</button>'));
            $fields->addFieldsToTab('Root.Main', TextareaField::create('TestimonialMarkup', 'Testimonial Markup')->addExtraClass('hide-testimonial-markup'));
            $testimonialItemArray = json_decode("[".$this->RemoveLinesAndTabsFromText($this->TestimonialMarkup)."]", true);
            $TableRowMarkup = "";
            $i = 1;
            if(is_array($testimonialItemArray) && count($testimonialItemArray) > 0)foreach($testimonialItemArray AS $TestimonialItem){
                $TableRowMarkup .= '<tr class="testimonial-item-'.$i.'" data-position="'.$i.'">';
                $TableRowMarkup .= '<td class="testimonial-name">'.$TestimonialItem['name'].'</td>';
                $TableRowMarkup .= '<td class="testimonial-company">'.$TestimonialItem['company'].'</td>';
                $TableRowMarkup .= '<td class="testimonial-content">'.$TestimonialItem['content'].'</td>';
                $TableRowMarkup .= '<td><a href="#" class="testimonial-edit">Edit</a></td>';
                $TableRowMarkup .= '<td><a href="#" class="testimonial-delete">Delete</a></td>';
                $TableRowMarkup .= '</tr>';
                $i++;
            }
            $TableVisibility = ' style="display:none;"';
            if(count($testimonialItemArray) > 0){
                $TableVisibility = ' style="display:block;"';
            }
            $fields->addFieldsToTab('Root.Main', new LiteralField('Literal2', '<div id="TestimonialElementContainer"'.$TableVisibility.'><table data-total-items="'.count($testimonialItemArray).'"><thead><tr><th>Name</th><th>Company</th><th>Content</th><th class="edit-header">Edit</th><th class="delete-header">Delete</th></tr></thead><tbody>'.$TableRowMarkup.'</tbody></table></div>'));
        });

        $fields = parent::getCMSFields();

        if ($this->isEndofLine('ElementContent') && $this->hasExtension('VersionViewerDataObject')) {
            $fields = $this->addVersionViewer($fields, $this);
        }

        return $fields;
    }

    public function getCssStyle()
    {
        $styles = $this->config()->get('styles');
        $style = $this->Style;

        if (isset($styles[$style])) {
            return strtolower($styles[$style]);
        }
    }

    /*****
     * Use this method to return the correct Markup for the Testimonial.
     */
    public function getHTMLMarkup() {
        $TestimonialItems = json_decode("[".$this->RemoveLinesAndTabsFromText($this->TestimonialMarkup)."]", true);
        $Markup = '';
        $i = 1;
        if(is_array($TestimonialItems) && count($TestimonialItems) > 0)foreach($TestimonialItems AS $TestimonialItem){

            $Markup .= '<div class="respond respond-blockquote border col-xs-12 col-sm-12 col-md-12">';
            $Markup .= '<div class="description border-info">';
            $Markup .= '<blockquote>';
            $Markup .= str_replace("&quot;", '"', ShortcodeParser::get_active()->parse($TestimonialItem['content']));
            $Markup .= '</blockquote>';
            $Markup .= '</div>';
            $Markup .= '<div class="name">';
            $Markup .= '<strong>'.$TestimonialItem['name'].'</strong>';
            $Markup .= '<div>'.nl2br($TestimonialItem['company']).'</div>';
            $Markup .= '</div>';
            $Markup .= '</div>';

            $i++;
        }
        return $Markup;
    }

    public function onBeforeWrite()
    {
        parent::onBeforeWrite();
        if($this->TestimonialMarkup)
        {
           $this->TestimonialMarkup = $this->RemoveLinesAndTabsFromText($this->TestimonialMarkup);
        }

    }

}