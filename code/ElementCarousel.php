<?php

/**
 * @package elemental
 */
class ElementCarousel extends BaseElement
{
    public static $db = array(
        'Heading' => 'Varchar(255)',
        'Autoscroll' => 'Boolean',
        'TopNavigation' => 'Boolean'
    );

    public static $many_many = array(
        'Images' => 'Image'
    );

    private static $styles = array();

    private static $title = "Carousel";

    private static $description = "This block will allow you to configure an image carousel";

    public function getCMSFields()
    {

        $this->beforeUpdateCMSFields(function ($fields) {
            $fields->removeByName('Style');
            $fields->addFieldsToTab('Root.Main', TextField::create('Heading', 'Carousel Heading'));
            $fields->addFieldsToTab('Root.Main', CheckboxField::create('TopNavigation', 'Navigation'));
            $UploadCarouselImages = new UploadField('Images', 'Upload your images for this Carousel');
            $UploadCarouselImages->setFolderName('theme-images/carousel-images');
            $fields->addFieldsToTab('Root.Main', $UploadCarouselImages);

        });

        $fields = parent::getCMSFields();

        if ($this->isEndofLine('ElementContent') && $this->hasExtension('VersionViewerDataObject')) {
            $fields = $this->addVersionViewer($fields, $this);
        }

        return $fields;
    }

    public function getCssStyle()
    {
        $styles = $this->config()->get('styles');
        $style = $this->Style;

        if (isset($styles[$style])) {
            return strtolower($styles[$style]);
        }
    }

    public function getCarouselHTML() {
        $CarouselImages = $this->Images();
        $html = '';
        foreach($CarouselImages AS $CarouselImage){

            $html .= '<div class="make-wrapper">';
            $html .= '<a href="'.$CarouselImage->Title.'" target="_blank" class="make">';
            $html .= '<img class="loaded" src="'.$CarouselImage->Url.'" width="128" height="128" alt="">';
            $html .= '</a>';
            $html .= '</div>';
        }
        return $html;
    }

}