<?php

/**
 * @package elemental
 */
class ElementPictureBox extends BaseElement
{

    private static $db = array(
        'LabelOne' => 'Varchar(10)',
        'DescriptionOne' => 'Varchar(255)',
        'LabelTwo' => 'Varchar(10)',
        'DescriptionTwo' => 'Varchar(255)',
        'LabelThree' => 'Varchar(10)',
        'DescriptionThree' => 'Varchar(255)'
    );

    private static $has_one = array(
        'BackgroundImage' => 'Image'
)   ;

    private static $styles = array();

    private static $title = "Picture Box";

    private static $description = "This block will allow you to configure a Picture Box";

    public function getCMSFields()
    {

        $this->beforeUpdateCMSFields(function ($fields) {
            $fields->removeByName('Style');
            $fields->addFieldsToTab('Root.Main', new TextField('LabelOne', 'Label Left Box'));
            $fields->addFieldsToTab('Root.Main', new TextField('DescriptionOne', 'Description Left Box'));
            $fields->addFieldsToTab('Root.Main', new TextField('LabelTwo', 'Label Centre Box'));
            $fields->addFieldsToTab('Root.Main', new TextField('DescriptionTwo', 'Description Centre Box'));
            $fields->addFieldsToTab('Root.Main', new TextField('LabelThree', 'Label Right Box'));
            $fields->addFieldsToTab('Root.Main', new TextField('DescriptionThree', 'Description Right Box'));
            $fields->addFieldsToTab('Root.Main', new TextField('DescriptionThree', 'Description Right Box'));
            $UploadDefaultBanner = new UploadField('BackgroundImage', 'Upload a background Image for this section (Dimensions: 1200px wide x 480px high)');
            $UploadDefaultBanner->setFolderName('theme-images/picture-box-images');
            $fields->addFieldsToTab('Root.Main', $UploadDefaultBanner);

        });

        $fields = parent::getCMSFields();

        if ($this->isEndofLine('ElementContent') && $this->hasExtension('VersionViewerDataObject')) {
            $fields = $this->addVersionViewer($fields, $this);
        }

        return $fields;
    }

    public function getCssStyle()
    {
        $styles = $this->config()->get('styles');
        $style = $this->Style;

        if (isset($styles[$style])) {
            return strtolower($styles[$style]);
        }
    }

    public function getTopPadding() {
        return (($this->BackgroundImage()->getDimensions(1)-($this->BackgroundImage()->getDimensions(1) * 0.1)) * 0.25);
    }

    public function getBottomPadding() {
        return (($this->BackgroundImage()->getDimensions(1)-($this->BackgroundImage()->getDimensions(1) * 0.1)) * 0.25);
    }

    public function getTopMargin() {
        return (($this->BackgroundImage()->getDimensions(1)-($this->BackgroundImage()->getDimensions(1) * 0.5)) * 0.25);
    }

}