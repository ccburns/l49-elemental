<?php

/**
 * @package elemental
 */
class ElementPositionReset extends BaseElement
{

    private static $db = array(
        'Style' => 'Varchar'
    );

    private static $styles = array();

    private static $title = "Element Position Reset";

    private static $description = "Position reset, clears any spacing and starts a brand new row";

    public function getCMSFields()
    {

        $styles = $this->config()->get('styles');

        if (count($styles) > 0) {
            $this->beforeUpdateCMSFields(function ($fields) use ($styles) {
                $styles->setEmptyString('Select a custom style..');
            });
        } else {
            $this->beforeUpdateCMSFields(function ($fields) {
                $fields->removeByName('Style');
            });
        }

        $fields = parent::getCMSFields();
        return $fields;
    }

    public function getCssStyle()
    {
        $styles = $this->config()->get('styles');
        $style = $this->Style;

        if (isset($styles[$style])) {
            return strtolower($styles[$style]);
        }
    }
}
