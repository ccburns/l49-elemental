<?php

/**
 * @package elemental
 */
class ElementGallery extends BaseElement
{

    private static $db = array(
        'Temp' => 'Varchar(255)'
    );

    private static $has_many = array(
        'ElementGalleryImages' => 'ElementGalleryImage'
    );

    private static $styles = array();

    private static $title = "Gallery Block";

    private static $description = "Upload images and provide a link to each uploaded images.";

    public function getCMSFields()
    {

        $this->beforeUpdateCMSFields(function ($fields) {
            $fields->removeByName('Style');
            $fields->addFieldsToTab('Root.Main', new HeaderField('Header1', 'Element Gallery Instructions', 3));
            $UploadElementImages = new UploadField('ElementGalleryImages', '<p class="message warning">Images will be cropped or padded to 250px wide and 150px high</p>');
            $UploadElementImages->setFolderName('theme-images/element-images');
            $fields->addFieldsToTab('Root.Main', $UploadElementImages);
        });

        $fields = parent::getCMSFields();

        if ($this->isEndofLine('ElementContent') && $this->hasExtension('VersionViewerDataObject')) {
            $fields = $this->addVersionViewer($fields, $this);
        }

        return $fields;
    }

    public function getCssStyle()
    {
        $styles = $this->config()->get('styles');
        $style = $this->Style;

        if (isset($styles[$style])) {
            return strtolower($styles[$style]);
        }
    }

//    public function checkIfLink($url) {
//        Debug::dump($url);
//        if(strpos($url, 'http://')return true;
//        else return false;
//    }

}