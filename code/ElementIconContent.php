<?php

/**
 * @package elemental
 */
class ElementIconContent extends BaseElement
{

    private static $db = array(
        'Link' => 'Varchar(255)',
        'IconCode' => 'Text',
        'Paragraph' => 'HTMLText',
        'Style' => 'Varchar'
    );

    private static $styles = array();

    private static $title = "Icon Element Code";

    private static $description = "Block of content that includes an icon and content";

    public function getCMSFields()
    {

        $styles = $this->config()->get('styles');

        if (count($styles) > 0) {
            $this->beforeUpdateCMSFields(function ($fields) use ($styles) {
                $fields->addFieldsToTab('Root.Main', TextareaField::create('IconCode', 'Link')->setDescription('Enter a link to where this whole section should take you. Leave blank if you don\'t want it to be a link'));
                $fields->addFieldsToTab('Root.Main', new TextareaField('IconCode', 'RAW Icon Code (HTML)'));
                $fields->addFieldsToTab('Root.Main', new HtmlEditorField('Paragraph', 'Paragraph'));

                $styles->setEmptyString('Select a custom style..');
            });
        } else {
            $this->beforeUpdateCMSFields(function ($fields) {
                $fields->removeByName('Style');
            });
        }

        $fields = parent::getCMSFields();

        if ($this->isEndofLine('ElementContent') && $this->hasExtension('VersionViewerDataObject')) {
            $fields = $this->addVersionViewer($fields, $this);
        }

        return $fields;
    }

    public function getCssStyle()
    {
        $styles = $this->config()->get('styles');
        $style = $this->Style;

        if (isset($styles[$style])) {
            return strtolower($styles[$style]);
        }
    }
}
