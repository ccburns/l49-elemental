<?php

/**
 * @package elemental
 */
class ElementDivision extends BaseElement
{

    private static $db = array(
        'DivisionTitle' => 'Varchar(255)',
        'DivisionDescription' => 'HTMLText',
        'DivisionBulletPoints' => 'Text',
        'LinkLocation' => 'Varchar(255)',
        'LinkText' => 'Varchar(255)'
    );

    private static $has_one = array(
        'DivisionImage' => 'Image'
    )   ;

    private static $styles = array();

    private static $title = "Division Block";

    private static $description = "This block will allow you to configure a Picture Box";

    public function getCMSFields()
    {

        $this->beforeUpdateCMSFields(function ($fields) {
            $fields->removeByName('Style');
            $fields->addFieldsToTab('Root.Main', new TextField('DivisionTitle', 'Division Name'));
            $fields->addFieldsToTab('Root.Main', new HtmlEditorField('DivisionDescription', 'Description'));
            $fields->addFieldsToTab('Root.Main', TextareaField::create('DivisionBulletPoints', 'Bullet Points')->setDescription('Enter each bullet point on a new line'));
            $fields->addFieldsToTab('Root.Main', TextField::create('LinkLocation', 'Division URL')->setDescription('Enter the full URL (including the http://) for the Division Website'));
            $fields->addFieldsToTab('Root.Main', new TextField('LinkText', 'Text to display on Button'));
            $UploadDivisionImage = new UploadField('DivisionImage', 'Upload a Division Image for this section (Dimensions: 580px wide x 330px high)');
            $UploadDivisionImage->setFolderName('theme-images/division-images');
            $fields->addFieldsToTab('Root.Main', $UploadDivisionImage);

        });

        $fields = parent::getCMSFields();

        if ($this->isEndofLine('ElementContent') && $this->hasExtension('VersionViewerDataObject')) {
            $fields = $this->addVersionViewer($fields, $this);
        }

        return $fields;
    }

    public function getCssStyle()
    {
        $styles = $this->config()->get('styles');
        $style = $this->Style;

        if (isset($styles[$style])) {
            return strtolower($styles[$style]);
        }
    }

    public function getDivisionBulletPointsHTML() {

        $BulletPoints = explode("\n", $this->DivisionBulletPoints);
        $html = '<ul class="division-bullet-point">';
        foreach($BulletPoints AS $BulletPoint){
            $html .= "<li>{$BulletPoint}</li>";
        }
        $html .= "</ul>";
        return $html;
    }
}