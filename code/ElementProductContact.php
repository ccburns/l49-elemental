<?php

/**
* Element Product Contact
*/
class ElementProductContact extends BaseElement
{
    private static $db = array(
        'PhoneNumber' => 'Varchar(255)',
        'EmailAddress' => 'Varchar(255)',
        'ItemID' => 'Varchar(255)',
    );

    private static $styles = array();

    private static $title = "Product Contact Block";

    private static $description = "This block will allow you to set up a Product Contact Block";

    public function getCMSFields()
    {

        $this->beforeUpdateCMSFields(function ($fields) {
            $fields->removeByName('Style');
            $fields->addFieldsToTab('Root.Main', new TextField('PhoneNumber', 'Phone Number'));
            $fields->addFieldsToTab('Root.Main', new EmailField('EmailAddress', 'Email Address'));
            $fields->addFieldsToTab('Root.Main', TextField::create('ItemID', 'ItemID')->setDescription('Product ItemID Eg: (MEMIC150)'));

        });

        $fields = parent::getCMSFields();

        if ($this->isEndofLine('ElementContent') && $this->hasExtension('VersionViewerDataObject')) {
            $fields = $this->addVersionViewer($fields, $this);
        }

        return $fields;
    }

    public function getCssStyle()
    {
        $styles = $this->config()->get('styles');
        $style = $this->Style;

        if (isset($styles[$style])) {
            return strtolower($styles[$style]);
        }
    }
}