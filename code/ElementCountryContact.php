<?php

/**
 * @package elemental
 */
class ElementCountryContact extends BaseElement
{

    private static $db = array(
        'CountryName' => 'Varchar(255)',
        'PhoneNumber' => 'Varchar(255)',
        'EmailAddress' => 'Varchar(255)',
        'LinkLocation' => 'Varchar(255)',
        'LinkText' => 'Varchar(255)'
    );

    private static $styles = array();

    private static $title = "Country Contact Block";

    private static $description = "This block will allow you to set up a Country Contact Block";

    public function getCMSFields()
    {

        $this->beforeUpdateCMSFields(function ($fields) {
            $fields->removeByName('Style');
            $fields->addFieldsToTab('Root.Main', new TextField('CountryName', 'Country Name'));
            $fields->addFieldsToTab('Root.Main', new TextField('PhoneNumber', 'Phone Number'));
            $fields->addFieldsToTab('Root.Main', new EmailField('EmailAddress', 'Email Address'));
            $fields->addFieldsToTab('Root.Main', TextField::create('LinkLocation', 'Link Location')->setDescription('Enter the full URL (including the http://) for where the link should go'));
            $fields->addFieldsToTab('Root.Main', new TextField('LinkText', 'Text to display on Button'));

        });

        $fields = parent::getCMSFields();

        if ($this->isEndofLine('ElementContent') && $this->hasExtension('VersionViewerDataObject')) {
            $fields = $this->addVersionViewer($fields, $this);
        }

        return $fields;
    }

    public function getCssStyle()
    {
        $styles = $this->config()->get('styles');
        $style = $this->Style;

        if (isset($styles[$style])) {
            return strtolower($styles[$style]);
        }
    }

//    public function getDivisionBulletPointsHTML() {
//
//        $BulletPoints = explode("\n", $this->DivisionBulletPoints);
//        $html = '<ul class="division-bullet-point">';
//        foreach($BulletPoints AS $BulletPoint){
//            $html .= "<li>{$BulletPoint}</li>";
//        }
//        $html .= "</ul>";
//        return $html;
//    }
}