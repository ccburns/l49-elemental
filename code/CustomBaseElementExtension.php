<?php

/**
* Custom Base Element Extension
*/
class CustomBaseElementExtension extends DataExtension
{
    
    /* 
    * Clean Newlines, Spaces and Tabs 
    */
    public function RemoveLinesAndTabsFromText($text){
        //remove newlines, spaces and tabs from the markup text
       return $newTabbedMarkup = trim(preg_replace("/[\r\n\t]*/","", $text));
    }
}