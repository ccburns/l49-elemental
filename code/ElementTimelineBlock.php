<?php

/**
 * @package elemental
 */
class ElementTimelineBlock extends BaseElement
{

    private static $db = array(
        'BlockHeader' => 'Varchar(255)',
        'TimelineMarkup' => 'HTMLText',
        'Style' => 'Varchar'
    );

    private static $styles = array();

    private static $title = "Timeline Block";

    private static $description = "This block will allow you to configure an vertical timeline on the website";

    public function getCMSFields()
    {

        $this->beforeUpdateCMSFields(function ($fields) {
            $fields->removeByName('Style');
            $fields->addFieldsToTab('Root.Main', new HeaderField('Header1', 'Timeline Block Content Instructions', 3));
            $fields->addFieldsToTab('Root.Main', new LiteralField('Literal1', '<p>Fill in the fields below and then click the <strong><em>Add Timeline Item</em></strong> button below it to add this content to the Timeline Block. Once clicked you will see the details in the table below the Button.</p>'));
            $dateField = new DateField('DateField', 'Date');
            $dateField->setConfig('showcalendar', true);
            $dateField->setConfig('showdropdown', true);
            $dateField->setConfig('dateformat', 'YYYY-mm-dd');
            $fields->addFieldsToTab('Root.Main', $dateField->setDescription('Will be displayed on the website as <strong>June 23rd, 2016</strong>'));
            $fields->addFieldsToTab('Root.Main', new HtmlEditorField('ContentField', 'Content'));
            $fields->addFieldToTab("Root.Main", DropdownField::create('BackgroundField', 'Background Type', array('Stroke'=>'Stroke (Boarder only)','Fill'=>'Fill (Background will be a block colour)'))->setValue('Stroke'));
            $fields->addFieldToTab("Root.Main", DropdownField::create('StyleField', 'Element Style', array('none'=>'None','danger'=>'Red (danger)','success'=>'Green (success)','warning'=>'Orange (warning)','primary'=>'Blue (primary)'))->setValue('none'));
            $fields->addFieldsToTab('Root.Main', new LiteralField('Button1', '<button class="add-timeline-button">Add Timeline Item</button>'));
            $fields->addFieldsToTab('Root.Main', new LiteralField('Button2', '<button class="update-timeline-button">Update Timeline Item</button>'));
            $fields->addFieldsToTab('Root.Main', new TextareaField('TimelineMarkup', 'TimeLine Markup'));

            $TimelineBlockArray = json_decode("[".$this->RemoveLinesAndTabsFromText($this->TimelineMarkup)."]", true);

            $TableRowMarkup = "";
            $i = 1;
            foreach($TimelineBlockArray AS $TimelineBlock){
                $TableRowMarkup .= '<tr class="timeline-item-'.$i.'" data-position="'.$i.'">';
                $TableRowMarkup .= '<td class="timeline-date">'.$TimelineBlock['date'].'</td>';
                $TableRowMarkup .= '<td class="timeline-content">'.$TimelineBlock['content'].'</td>';
                $TableRowMarkup .= '<td class="timeline-background-type">'.$TimelineBlock['background-type'].'</td>';
                $TableRowMarkup .= '<td class="timeline-background-style">'.$TimelineBlock['background-style'].'</td>';
                $TableRowMarkup .= '<td><a href="#" class="timeline-edit">Edit</a></td>';
                $TableRowMarkup .= '<td><a href="#" class="timeline-delete">Delete</a></td>';
                $TableRowMarkup .= '</tr>';
                $i++;
            }
            $TableVisibility = ' style="display:none;"';
            if(count($TimelineBlockArray) > 0){
                $TableVisibility = ' style="display:block;"';
            }

            $fields->addFieldsToTab('Root.Main', new LiteralField('Literal3', '<div id="TimelineBlockElementContainer"'.$TableVisibility.'><table data-total-items="'.count($TimelineBlockArray).'"><thead><tr><th>Date</th><th>Content</th><th>Stroke/Fill</th><th>Style</th><th class="edit-header">Edit</th><th class="delete-header">Delete</th></tr></thead><tbody>'.$TableRowMarkup.'</tbody></table></div>'));
        });

        $fields = parent::getCMSFields();

        if ($this->isEndofLine('ElementContent') && $this->hasExtension('VersionViewerDataObject')) {
            $fields = $this->addVersionViewer($fields, $this);
        }

        return $fields;
    }

    public function getCssStyle()
    {
        $styles = $this->config()->get('styles');
        $style = $this->Style;

        if (isset($styles[$style])) {
            return strtolower($styles[$style]);
        }
    }

    /*****
     * Use this method to return the correct Markup for the Accordian.
     */
    public function getHTMLMarkup() {

        $TimelineBlocks = json_decode("[".$this->RemoveLinesAndTabsFromText($this->TimelineMarkup)."]", true);
        $Markup = '<div class="timeline">';
        foreach($TimelineBlocks AS $TimelineBlock) {
            if($TimelineBlock['background-type'] == 'Fill'){
                $styling = " bg bg-".$TimelineBlock['background-style'];
                $iconStyling = " bg-".$TimelineBlock['background-style'];
            }elseif($TimelineBlock['background-type'] == 'Stroke'){
                $styling = " border border-".$TimelineBlock['background-style'];
                $iconStyling = " bg-".$TimelineBlock['background-style'];
            }else{
                $styling = " ";
                $iconStyling = " ";
            }
            $Markup .= '<article class="post">';
            $Markup .= '<div class="timeline-time">';
            $Markup .= '<time datetime="'.$TimelineBlock['date'].'">'.date("F, Y", strtotime($TimelineBlock['date'])).'</time>';
            $Markup .= '</div>';
            $Markup .= '<div class="timeline-icon'.$iconStyling.'">';
            $Markup .= '<div class="livicon" data-n="spinner-six" data-c="#fff" data-hc="0" data-s="22"></div>';
            $Markup .= '</div>';
            $Markup .= '<div class="timeline-content'.$styling.'" data-appear-animation="fadeInRight">';
            $Markup .= '<div class="entry-content">';
            $Markup .= ShortcodeParser::get_active()->parse($TimelineBlock['content']);
            $Markup .= '</div>';
            $Markup .= '</div>';
            $Markup .= '</article>';

        }
        $Markup .= '</div>';
        return $Markup;
    }
}