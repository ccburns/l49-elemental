<?php

/**
 * @package elemental
 */
class ElementAccordian extends BaseElement
{

    private static $db = array(
        'BlockHeader' => 'Varchar(255)',
        'AccordianMarkup' => 'HTMLText',
        'Style' => 'Varchar'
    );

    private static $styles = array();

    private static $title = "Accordian Block";

    private static $description = "This block will allow you to configure an Accordian";

    public function getCMSFields()
    {

        $this->beforeUpdateCMSFields(function ($fields) {
            $fields->removeByName('Style');
            $fields->addFieldsToTab('Root.Main', new HeaderField('Header1', 'Accordian Content Instructions', 3));
            $fields->addFieldsToTab('Root.Main', new LiteralField('Literal1', '<p>Fill in the Label and Content fields below and then click the <strong><em>Add Accordian Item</em></strong> button below it to add this content to the Accordian. Once clicked you will see the details in the table below the Button.</p>'));
            $fields->addFieldsToTab('Root.Main', new TextField('LabelField', 'Label'));
            $fields->addFieldsToTab('Root.Main', new HtmlEditorField('ContentField', 'Content'));
            $fields->addFieldsToTab('Root.Main', new LiteralField('Button1', '<button class="add-accordian-button">Add Accordian Item</button>'));
            $fields->addFieldsToTab('Root.Main', new LiteralField('Button2', '<button class="update-accordian-button">Update Accordian Item</button>'));
            $fields->addFieldsToTab('Root.Main', new HiddenField('AccordianMarkup', 'Accordian Markup'));
            $AccordianItemArray = json_decode("[".$this->RemoveLinesAndTabsFromText($this->AccordianMarkup)."]", true);
            $TableRowMarkup = "";
            $i = 1;
            foreach($AccordianItemArray AS $AccordianItem){
                $TableRowMarkup .= '<tr class="accordian-item-'.$i.'" data-position="'.$i.'">';
                $TableRowMarkup .= '<td class="accordian-label">'.$AccordianItem['label'].'</td>';
                $TableRowMarkup .= '<td class="accordian-content">'.$AccordianItem['content'].'</td>';
                $TableRowMarkup .= '<td><a href="#" class="accordian-edit">Edit</a></td>';
                $TableRowMarkup .= '<td><a href="#" class="accordian-delete">Delete</a></td>';
                $TableRowMarkup .= '</tr>';
                $i++;
            }
            $TableVisibility = ' style="display:none;"';
            if(count($AccordianItemArray) > 0){
                $TableVisibility = ' style="display:block;"';
            }
            $fields->addFieldsToTab('Root.Main', new LiteralField('Literal2', '<div id="AccordianElementContainer"'.$TableVisibility.'><table data-total-items="'.count($AccordianItemArray).'"><thead><tr><th>Label</th><th>Content</th><th class="edit-header">Edit</th><th class="delete-header">Delete</th></tr></thead><tbody>'.$TableRowMarkup.'</tbody></table></div>'));
        });

        $fields = parent::getCMSFields();

        if ($this->isEndofLine('ElementContent') && $this->hasExtension('VersionViewerDataObject')) {
            $fields = $this->addVersionViewer($fields, $this);
        }

        return $fields;
    }

    public function getCssStyle()
    {
        $styles = $this->config()->get('styles');
        $style = $this->Style;

        if (isset($styles[$style])) {
            return strtolower($styles[$style]);
        }
    }

    /*****
     * Use this method to return the correct Markup for the Accordian.
     */
    public function getHTMLMarkup() {
        $AccordianItems = json_decode("[".$this->RemoveLinesAndTabsFromText($this->AccordianMarkup)."]", true);
        $Markup = '<div class="panel-group" id="accordion-'.$this->ID.'">';
        $i = 1;
        foreach($AccordianItems AS $AccordianItem){
            $Markup .= '<div class="panel panel-default">';
            $Markup .= '<div class="panel-heading">';
            $Markup .= '<div class="panel-title">';
            $Markup .= '<a data-toggle="collapse" data-parent="#accordion" href="#collapse'.$i.'-'.$this->ID.'">';
            $Markup .= $AccordianItem['label'];
            $Markup .= '</a>';
            $Markup .= '</div>';
            $Markup .= '</div>';
            $Markup .= '<div id="collapse'.$i.'-'.$this->ID.'" class="panel-collapse collapse">';
            $Markup .= '<div class="panel-body">';
            $Markup .= ShortcodeParser::get_active()->parse($AccordianItem['content']);
            $Markup .= '</div>';
            $Markup .= '</div>';
            $Markup .= '</div>';
            $i++;
        }
        $Markup .= '</div>';
        return $Markup;
    }

    public function onBeforeWrite()
    {
        parent::onBeforeWrite();
        if($this->AccordianMarkup)
        {
           $this->AccordianMarkup =  $this->RemoveLinesAndTabsFromText($this->AccordianMarkup);
        }

    }
}