<?php

/**
 * @package elemental
 */
class ElementRecentBlogPosts extends BaseElement
{

    private static $db = array(
        'BlockHeader' => 'Varchar(255)',
        'BlogLink' => 'Varchar(255)',
        'Style' => 'Varchar'
    );

    private static $styles = array();

    private static $title = "Recent Blogs Block";

    private static $description = "Block displaying 4 of the most recent blog posts";

    public function getCMSFields()
    {

        $styles = $this->config()->get('styles');

        if (count($styles) > 0) {
            $this->beforeUpdateCMSFields(function ($fields) use ($styles) {
                $fields->addFieldsToTab('Root.Main', $styles = new DropdownField('Style', 'Style', $styles));

                $styles->setEmptyString('Select a custom style..');
            });
        } else {
            $this->beforeUpdateCMSFields(function ($fields) {
                $fields->removeByName('Style');
            });
        }

        $fields = parent::getCMSFields();

        if ($this->isEndofLine('ElementContent') && $this->hasExtension('VersionViewerDataObject')) {
            $fields = $this->addVersionViewer($fields, $this);
        }

        return $fields;
    }

    public function getCssStyle()
    {
        $styles = $this->config()->get('styles');
        $style = $this->Style;

        if (isset($styles[$style])) {
            return strtolower($styles[$style]);
        }
    }

    function getRecentBlogPosts() {
        $BlogPosts = BlogPost::get()->sort('PublishDate DESC')->limit(4);
        return $BlogPosts;
    }

}
