<?php

class ElementGalleryImage extends Image {

    private static $db = array(
        'Link' => 'Varchar(255)'
    );

    public static $has_one = array(
        'ElementGallery' => 'ElementGallery'
    );
}