<?php

/**
 * @package elemental
 */
class ElementTeamMember extends BaseElement
{

    private static $db = array(
        'FullName'=>'Varchar(255)',
        'Position'=>'Varchar(255)',
        'Biography'=>'HTMLText'
    );

    private static $has_one = array(
        'Page' => 'Page',
        'BiographyImage' => 'Image'
    );

    private static $styles = array();

    private static $title = "Team Member Block";

    private static $description = "This block will allow you to configure a Team Member Block... You can choose for it to be either half or full width";

    public function getCMSFields()
    {

        $this->beforeUpdateCMSFields(function ($fields) {
            $fields->removeByName('Style');

            $PageID = Controller::curr()->currentPageID();

            $fields->addFieldToTab("Root.Main", HeaderField::create('Header1', 'Team Member', 3));
            $fields->addFieldToTab("Root.Main", HiddenField::create('PageID', 'PageID', $PageID));
            $fields->addFieldToTab("Root.Main", TextField::create('FullName', 'Display Name'));
            $fields->addFieldToTab("Root.Main", TextField::create('Position', 'Position'));
            $fields->addFieldToTab("Root.Main", HtmlEditorField::create('Biography', 'Biography'));
            $fields->addFieldToTab("Root.Main", LiteralField::create('Literal1', '<p class="message warning">Team Member Image (Dimensions: 300px wide x 300px high)</p>'), 'BiographyImage');
            $UploadTeamImage = new UploadField('BiographyImage', 'Upload a Team Member Image for this section (Dimensions: 300px wide x 300px high)');
            $UploadTeamImage->setFolderName('theme-images/team-images');
            $fields->addFieldsToTab('Root.Main', $UploadTeamImage);

        });

        $fields = parent::getCMSFields();

        if ($this->isEndofLine('ElementContent') && $this->hasExtension('VersionViewerDataObject')) {
            $fields = $this->addVersionViewer($fields, $this);
        }

        return $fields;
    }

    public function getCssStyle()
    {
        $styles = $this->config()->get('styles');
        $style = $this->Style;

        if (isset($styles[$style])) {
            return strtolower($styles[$style]);
        }
    }

}