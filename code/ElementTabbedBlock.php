<?php

/**
 * @package elemental
 */
class ElementTabbedBlock extends BaseElement
{

    private static $db = array(
        'BlockHeader' => 'Varchar(255)',
        'TabPosition' => "Enum('Top, Left, Right', 'Top')",
        'TabbedMarkup' => 'HTMLText',
        'Style' => 'Varchar'
    );

    private static $styles = array();

    private static $title = "Tabbed Block";

    private static $description = "This block will allow you to configure an Tabbed Block on the website";

    public function getCMSFields()
    {

        $this->beforeUpdateCMSFields(function ($fields) {
            $fields->removeByName('Style');
            $fields->addFieldsToTab('Root.Main', new HeaderField('Header1', 'Tabbed Block Content Instructions', 3));
            $fields->addFieldsToTab('Root.Main', new LiteralField('Literal1', '<p>Fill in the Label and Content fields below and then click the <strong><em>Add Tabbed Item</em></strong> button below it to add this content to the Tabbed Block. Once clicked you will see the details in the table below the Button.</p><p class="message warning">Be aware that the Labels length will be dependent on the size of the parent block (i.e. half the width of the screen, full width etc). It is best to use <strong>single word</strong> labels</p>'));
            $fields->addFieldsToTab('Root.Main', new TextField('LabelField', 'Label'));
            $fields->addFieldsToTab('Root.Main', new LiteralField('Literal2', '<p class="message warning">Font Awesome icons can be found at - <a href="http://fontawesome.io/cheatsheet/" target="_blank">http://fontawesome.io/cheatsheet/</a></p>'));
            $fields->addFieldsToTab('Root.Main', TextField::create('IconField', 'Icon for Label')->setDescription('To add an icon before the label enter a FontAwesome value here (e.g. fa-gears). See list above for a website where you can find all the icons'));
            $fields->addFieldsToTab('Root.Main', new HtmlEditorField('ContentField', 'Content'));
            $fields->addFieldsToTab('Root.Main', new LiteralField('Button1', '<button class="add-tabbed-button">Add Tabbed Block</button>'));
            $fields->addFieldsToTab('Root.Main', new LiteralField('Button2', '<button class="update-tabbed-button">Update Tabbed Block</button>'));
            $fields->addFieldsToTab('Root.Main', new TextareaField('TabbedMarkup', 'Tabbed Markup'));
            $TabbedBlockArray = json_decode("[".$this->TabbedMarkup."]", true);
            $TableRowMarkup = "";
            $i = 1;
            foreach($TabbedBlockArray AS $TabbedBlock){
                $TableRowMarkup .= '<tr class="tabbed-item-'.$i.'" data-position="'.$i.'">';
                $TableRowMarkup .= '<td class="tabbed-label">'.$TabbedBlock['label'].'</td>';
                $TableRowMarkup .= '<td class="tabbed-icon">'.$TabbedBlock['icon'].'</td>';
                $TableRowMarkup .= '<td class="tabbed-content">'.$TabbedBlock['content'].'</td>';
                $TableRowMarkup .= '<td><a href="#" class="tabbed-edit">Edit</a></td>';
                $TableRowMarkup .= '<td><a href="#" class="tabbed-delete">Delete</a></td>';
                $TableRowMarkup .= '</tr>';
                $i++;
            }
            $TableVisibility = ' style="display:none;"';
            if(count($TabbedBlockArray) > 0){
                $TableVisibility = ' style="display:block;"';
            }
            $fields->addFieldsToTab('Root.Main', new LiteralField('Literal3', '<div id="TabbedBlockElementContainer"'.$TableVisibility.'><table data-total-items="'.count($TabbedBlockArray).'"><thead><tr><th>Label</th><th>Icon</th><th>Content</th><th class="edit-header">Edit</th><th class="delete-header">Delete</th></tr></thead><tbody>'.$TableRowMarkup.'</tbody></table></div>'));
        });

        $fields = parent::getCMSFields();

        if ($this->isEndofLine('ElementContent') && $this->hasExtension('VersionViewerDataObject')) {
            $fields = $this->addVersionViewer($fields, $this);
        }

        return $fields;
    }

    public function getCssStyle()
    {
        $styles = $this->config()->get('styles');
        $style = $this->Style;

        if (isset($styles[$style])) {
            return strtolower($styles[$style]);
        }
    }

    /*****
     * Use this method to return the correct Markup for the Accordian.
     */
    public function getHTMLMarkup() {
        $TabbedBlocks = json_decode("[".$this->RemoveLinesAndTabsFromText($this->TabbedMarkup)."]", true);
        if($this->TabPosition == 'Top'){
            $Markup = '<div class="tabs">';
        }elseif($this->TabPosition == 'Left'){
            $Markup = '<div class="tabs tabs-left">';
        }elseif($this->TabPosition == 'Right'){
            $Markup = '<div class="tabs tabs-right">';
        }else{
            $Markup = '<div class="tabs">';
        }
        $i = 1;
        $LabelMarkup = '<ul class="nav nav-tabs">';
        $ContentMarkup = '<div class="tab-content">';
        foreach($TabbedBlocks AS $TabbedBlock){
            $labelAsAnchor = strtolower(preg_replace('~[^a-zA-Z0-9]+~', '', $TabbedBlock['label'])).'-'.$this->ID;
            if($i == 1){
                $LabelMarkup .= '<li class="active">';
                $ContentMarkup .= '<div class="tab-pane active fade in" id="'.$labelAsAnchor.'">';
            }else{
                $LabelMarkup .= '<li>';
                $ContentMarkup .= '<div class="tab-pane fade in" id="'.$labelAsAnchor.'">';
            }

			$LabelMarkup .= '<a href="#'.$labelAsAnchor.'" data-toggle="tab"><span class="open-sub"></span><i class="fa '.$TabbedBlock['icon'].'"></i> '.$TabbedBlock['label'].'</a>';
			$LabelMarkup .= '</li>';
			
            $ContentMarkup .= ShortcodeParser::get_active()->parse($TabbedBlock['content']);
            $ContentMarkup .= '</div>';
            $i++;
        }
        $LabelMarkup .= '</ul>';
        $ContentMarkup .= '</div>';

        $Markup .= $LabelMarkup.$ContentMarkup;
        $Markup .= '</div>';
        return $Markup;
    }
   
    public function onBeforeWrite()
    {
        parent::onBeforeWrite();
        if($this->TabbedMarkup)
        {
           $this->TabbedMarkup =  $this->RemoveLinesAndTabsFromText($this->TabbedMarkup);
        }

    }

}