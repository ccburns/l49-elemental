<?php

/**
 * @package elemental
 */
class ElementHeader extends BaseElement
{

    private static $db = array(
        'BlockHeader' => 'Varchar(255)',
        'HeaderSize' => "Enum('1,2,3,4,5,6',1)",
        'Style' => 'Varchar'
    );

    private static $styles = array();

    private static $title = "Element Header";

    private static $description = "Header Element (ability to choose header size)";

    public function getCMSFields()
    {

        $styles = $this->config()->get('styles');

        if (count($styles) > 0) {
            $this->beforeUpdateCMSFields(function ($fields) use ($styles) {
                $fields->addFieldsToTab('Root.Main', new DropdownField('HeaderSize', 'Content', array('1'=>'Heading 1','2'=>'Heading 2')));
                $fields->addFieldsToTab('Root.Main', $styles = new DropdownField('Style', 'Style', $styles));

                $styles->setEmptyString('Select a custom style..');
            });
        } else {
            $this->beforeUpdateCMSFields(function ($fields) {
                $fields->removeByName('Style');
                $fields->addFieldsToTab('Root.Main', new DropdownField('HeaderSize', 'Content', array('1'=>'Heading 1','2'=>'Heading 2')));
            });
        }

        $fields = parent::getCMSFields();

        if ($this->isEndofLine('ElementContent') && $this->hasExtension('VersionViewerDataObject')) {
            $fields = $this->addVersionViewer($fields, $this);
        }

        return $fields;
    }

    public function getCssStyle()
    {
        $styles = $this->config()->get('styles');
        $style = $this->Style;

        if (isset($styles[$style])) {
            return strtolower($styles[$style]);
        }
    }

    public function getModifiedBlockHeader() {
        $PlaceHolderProductsButton = "[[BTN View Products]]";
        $ReplacementHTMLProductsButton = '<a href="#" class="btn products-website-button"><i class="livicon" data-n="tags" data-s="20" data-c="#ffffff" data-hc="#ffffff"></i> View Products</a>';
        return str_replace($PlaceHolderProductsButton, $ReplacementHTMLProductsButton, $this->BlockHeader);
    }
}
